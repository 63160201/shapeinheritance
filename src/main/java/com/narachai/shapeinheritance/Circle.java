/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeinheritance;

/**
 *
 * @author ASUS
 */
public class Circle extends Shape{
    private double pi = 22.0/7;
    private double r;
    public Circle(double r){
        super(r,r);
        System.out.println("Circle created");
        this.r = r;
    }
    
    @Override
    public double calArea(){
        return pi*r*r;
    }
    
    @Override
    public void print(){
        System.out.println("Circle = "+calArea()+" r = "+r);
    }
    
}
