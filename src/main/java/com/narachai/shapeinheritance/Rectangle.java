/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeinheritance;

/**
 *
 * @author ASUS
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    public Rectangle(double width ,double height){
        super(width,height);
        System.out.println("Rectangle created");
        this.width = width;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return width*height;
    }
    
    @Override
    public void print(){
        System.out.println("Rectangle = "+calArea()+" width = "+width+" height = "+height);
    } 
}
