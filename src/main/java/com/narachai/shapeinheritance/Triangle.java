/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeinheritance;

/**
 *
 * @author ASUS
 */
public class Triangle extends Shape{
    private double h;
    private double b;
    public Triangle(double h ,double b){
        super(h,b);
        System.out.println("Triangle created");
        this.h = h;
        this.b = b;
    }
    
    @Override
    public double calArea(){
        return 0.5*h*b;
    }
    
    @Override
    public void print(){
        System.out.println("Triangle = "+calArea()+" h = "+h+" b = "+b);
    } 
}
