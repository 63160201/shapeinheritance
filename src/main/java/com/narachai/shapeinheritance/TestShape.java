/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeinheritance;

/**
 *
 * @author ASUS
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape(1,2);
        shape.calArea();
        shape.print();
        
        Circle circle = new Circle(3);
        circle.calArea();
        circle.print();
        
        Triangle triangle  = new Triangle(3,5);
        triangle.calArea();
        triangle.print();
        
        Rectangle rectangle  = new Rectangle(4,5);
        rectangle.calArea();
        rectangle.print();
        
        Square square  = new Square(5);
        square.calArea();
        square.print();
        
        
        System.out.println("Circle is Shape: "+(circle instanceof Shape));
        System.out.println("Triangle is Shape: "+(triangle instanceof Shape));
        System.out.println("Rectangle is Shape: "+(rectangle instanceof Shape));
        System.out.println("Square is Shape: "+(square instanceof Shape));
        
        Shape[] shapes = {circle, triangle ,rectangle  ,square};
        for(int i=0;i<shapes.length; i++){
            System.out.println("------------------------");
            shapes[i].print();
            
        }
    }
}
